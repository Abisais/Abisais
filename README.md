- 👋 Hi, I’m @Abisais
- 👀 I’m interested in engineering, energy, programming
- 🌱 I’m currently learning java, html, css, js, python
- 💞️ I’m looking to collaborate on java and python projects
- 📫 How to reach me: enmanuel_duarte@outlook.com
- www.linkedin.com/in/enmanuelduarteenergia
- Hobbies: read, play videogames, learning and smile!
<!---
Abisais/Abisais is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
